import win32api
import os
import kivy
import smartsheet
import logging
from kivy.lang import Builder
from kivy.app import App
from kivy.uix.screenmanager import ScreenManager, Screen 
from kivy.properties import ObjectProperty
from kivy.uix.boxlayout import BoxLayout 
from kivy.uix.screenmanager import FadeTransition
from kivy.graphics import Color, Rectangle
from kivy.uix.listview import ListItemButton
from kivy.uix.listview import ListView
from kivy.uix.progressbar import ProgressBar
from kivy.clock import Clock
from kivy.uix.tabbedpanel import TabbedPanel
from kivy.uix.dropdown import DropDown
from kivy.uix.popup import Popup
from kivy.uix.label import Label

token  = ""
matt_workspace = 7813091611174788
backup_workspace = 7266552727791492
ss_client = None
smartsheet.Smartsheet("04q0surnawg77afx11lvd6l0a2")

def get_index(text):
	result = str(text)[str(text).find(''):str(text).find('.')]
	return str(result)

class WelcomeScreen(Screen):
	global workspaces_name_and_id
	workspaces_name_and_id= []
	global apikey
	global backup_workspace
	#enter and save api key
	def get_saved_api(self):
		file = open("API.txt",'r')
		apikey = file.read()
		print(apikey)
		return str(apikey)
	#getting previously saved backup wkspc id
	def get_saved_wkspc(self):
		file = open("Backup.txt",'r')
		bkupwkspc = file.read()
		backup_workspace = bkupwkspc
		print(bkupwkspc)
		return str(bkupwkspc)

	def save_api(self, entry):
		file = open("API.txt", 'w')
		file.write(str(entry))
		file.close()

	def save_backup(self, entry):
		file = open("Backup.txt", 'w')
		file.write(str(entry))
		file.close()

	def api_next(self, entry):
		global token
		token = str(entry)
		global ss_client

		ss_client = smartsheet.Smartsheet(token)
		ss_client.errors_as_exceptions(True)

		workspace_response = ss_client.Workspaces.list_workspaces(include_all=True)
		all_workspaces = workspace_response.data
	
		if len(workspaces_name_and_id) ==  0:
			for x in all_workspaces:
				workspaces_name_and_id.append(([str(x.name)], x.id))
		if len(workspaces_name_and_id) > 0:
			print('IT WORKED BOSS')
		return token, ss_client, workspaces_name_and_id
class WorkspaceSelection(Screen):
	#buttons -> back, gather workspaces, next
	#loads and allows you to select wkspc/s to work with

	workspace_list = ObjectProperty()

	def gather_workspaces(self):
		del self.workspace_list.adapter.data[:]
		for x in workspaces_name_and_id:
			#add to the list view
			print(x)
			self.workspace_list.adapter.data.extend(['{}. {}'.format(workspaces_name_and_id.index(x), str(x[0]))])
		#reset the list view
		self.workspace_list._trigger_reset_populate()


	def set_selected_workspaces(self):
		global selected_workspaces
		global workspace_folders 
		global selected_workspaces_ID 
		global workspaces_with_no_folders 
		global selected_folders_id 

		selected_workspaces = []
		workspace_folders = []
		selected_workspaces_ID = []
		workspaces_with_no_folders = []
		selected_folders_id = []
		selection = self.workspace_list.adapter.selection
		if selection:
			for x in selection:
				print(x.text)
				selected_workspaces.append(workspaces_name_and_id[int(get_index(x.text))])
				print(' ')
				

			print(' ')
			print(selection)
			print(' ')	
			print(selected_workspaces)	


			#print(self.workspace_list.adapter.selection, '\n')
		return selected_workspaces

	#gets the folder data for next page
	def get_folders(self):
		#get workspace Id's
		for x in selected_workspaces:
			selected_workspaces_ID.append(x[1])
		for x in selected_workspaces_ID:
			templist = ss_client.Workspaces.list_folders(x, include_all = True).data
			if templist == []:
				workspaces_with_no_folders.append(x)
			else:
				for x in templist:
					workspace_folders.append((str(x.name), x.id))
		for x in workspace_folders:
			templist2 = ss_client.Folders.list_folders(x[1]).data
			if templist2 != []:
				for x in templist2:
					workspace_folders.append((str(x.name), x.id))
		print(workspace_folders)
class FolderSelection(Screen):
	folder_list = ObjectProperty()

	def folder_listing(self):
		del self.folder_list.adapter.data[:]
		for value in workspace_folders:
			self.folder_list.adapter.data.extend(['{}. {}'.format(str(workspace_folders.index(value)) , (str(value[0])))])

	    #print(workspaces_with_no_folders, "\n", workspace_folders)


	def set_selected_folders(self):
		selected_folders_id.clear()
		selection = self.folder_list.adapter.selection
		for x in selection:
			selected_folders_id.append(workspace_folders[int(get_index(x.text))][1])
		print(selected_folders_id)
		return workspace_folders, selected_folders_id

	def get_sheets(self):
		global wanted_sheets
		global selected_sheets_ID
		wanted_sheets = []
		selected_sheets_ID = []

		#accounts for workspaces with no folders
		if len(wanted_sheets) == 0:
			for x in workspaces_with_no_folders:
				wkspc_response = ss_client.Workspaces.get_workspace(x)
				wkpc_sheets = wkspc_response.sheets
				for x in wkpc_sheets:
					wanted_sheets.append((x.name, x.id))
			#accounts for workspaces with folders
			for x in selected_folders_id:
				fldr_response = ss_client.Folders.get_folder(x)
				fldr_sheets = fldr_response.sheets
				for x in fldr_sheets:
					wanted_sheets.append((x.name, x.id))

			#for wkpcs with folders and top level sheets
			for x in selected_workspaces:
				wkspc_toplvl_response	= ss_client.Workspaces.get_workspace(x[1])
				wkspc_toplvl_sheets = wkspc_toplvl_response.sheets
				for x in wkspc_toplvl_sheets:
					if (x.name, x.id) in wanted_sheets:
						pass
					else:
						wanted_sheets.append((x.name, x.id))

		print (wanted_sheets)
		print (len(wanted_sheets))
		return wanted_sheets
class SheetSelection(Screen):

	def list_sheets(self):
		del self.sheet_list.adapter.data[:]
		for x in wanted_sheets:
			self.sheet_list.adapter.data.extend(['{}. {}'.format(wanted_sheets.index(x), x[0])])

	def set_selected_sheets(self):
		selected_sheets_ID.clear()
		selection = self.sheet_list.adapter.selection
		if len(selected_sheets_ID) == 0:
			for x in selection:
				selected_sheets_ID.append(wanted_sheets[int(get_index(x.text))][1])

		print (selected_sheets_ID)
		return selected_sheets_ID
class Report(Screen):
	def sheet_report(self):
		global alike_sheets_by_row 
		global alike_sheets_by_row_sorted
		global alike_sheets_by_column
		global alike_sheets_by_column_sorted

		alike_sheets_by_row = {}
		alike_sheets_by_row_sorted = {}
		alike_sheets_by_column = {}
		alike_sheets_by_column_sorted = {}

		for x in selected_sheets_ID:

			sheet_response = ss_client.Sheets.get_sheet(x)
			sheet_column_length = len(sheet_response.columns)
			sheet_row_length = len(sheet_response.rows)
			sheet_name = sheet_response.name
			sheet_id = sheet_response.id
			#gets sheets alike by row
			if len(alike_sheets_by_row) == 0:
				alike_sheets_by_row[sheet_row_length] = [(sheet_name, sheet_row_length, sheet_id)]
				alike_sheets_by_row_sorted[sheet_row_length] = [sheet_name]
			elif alike_sheets_by_row.get(sheet_row_length) == None:
				alike_sheets_by_row[sheet_row_length] = [(sheet_name, sheet_row_length, sheet_id)]
				alike_sheets_by_row_sorted[sheet_row_length] = [sheet_name]
			elif alike_sheets_by_row[sheet_row_length] != 0:
				alike_sheets_by_row[sheet_row_length].append((sheet_name, sheet_row_length, sheet_id))
				alike_sheets_by_row_sorted[sheet_row_length].append(sheet_name)
				#gets sheets alike by column
			if len(alike_sheets_by_column) == 0:
				alike_sheets_by_column[sheet_column_length] = [(sheet_name, sheet_column_length, sheet_id)]
				alike_sheets_by_column_sorted[sheet_column_length] = [sheet_name]
			elif alike_sheets_by_column.get(sheet_column_length)== None:
				alike_sheets_by_column[sheet_column_length] = [(sheet_name, sheet_column_length, sheet_id)]
				alike_sheets_by_column_sorted[sheet_column_length] = [sheet_name]
			elif alike_sheets_by_column[sheet_column_length] != 0:
				alike_sheets_by_column[sheet_column_length].append((sheet_name, sheet_column_length, sheet_id))
				alike_sheets_by_column_sorted[sheet_column_length].append(sheet_name)

			print("{} \n Columns: {}, Rows: {}".format(sheet_response.name,len(sheet_response.columns), len(sheet_response.rows)))
			print(" ")
			print(alike_sheets_by_row)
	def report_results(self):
		list_messages = []
		index = 0
		del self.report_list.adapter.data[:]
		if len(alike_sheets_by_row) > 0 :
			for key, value in alike_sheets_by_row.items():
				print (key, value)
				self.report_list.adapter.data.extend(["{}. The following sheets have {} Rows: \n {}".format(index, key, alike_sheets_by_row_sorted[key])])
				index += 1
			for key, value in alike_sheets_by_column.items():
				print(key,value)
				self.report_list.adapter.data.extend(["{}. The following sheets have {} Columns: \n {}".format(index, key, alike_sheets_by_column_sorted[key])])	
				index += 1

		else:
			print('No Report Ran Yet')
	def report_remove_sheets(self):
		'''for x in selected_sheets_ID:
			sheet_response = ss_client.Sheets.get_sheet(x)
			sheet_name = sheet_response.name
			for items in listbox.curselection():
				return True'''
		selection = self.report_list.adapter.selection
		print(selected_sheets_ID)
		report_dictionaries = []
		report_dictionaries_final = []
		for x in alike_sheets_by_row_sorted:
			report_dictionaries.append(x)
		for x in alike_sheets_by_column_sorted:
			report_dictionaries.append(x)
		print(report_dictionaries)
		for x in selection:
			report_dictionaries_final.append(report_dictionaries[int(get_index(x.text))])
		print(report_dictionaries_final)	

		for item in report_dictionaries_final:
			for key, value in alike_sheets_by_row.items():

				if str(key) == str(item):
					for sheet in value:
						if sheet[2] in selected_sheets_ID:
							selected_sheets_ID.remove(sheet[2])

		for item in report_dictionaries_final:
			for key, value in alike_sheets_by_column.items():	
				if str(key) == str(item):
					for sheet in value:
						if sheet[2] in selected_sheets_ID:
							selected_sheets_ID.remove(sheet[2])

		print(selected_sheets_ID)

class SheetEditor(Screen):

	def add_row(self):
		#create row object
		
		row_storage = {}
		column_storage = {}
		for ID in selected_sheets_ID:
			#set up sheet/row/col data
			sheet = ss_client.Sheets.get_sheet(ID)
			sheet_name = sheet.name
			sheet_rows = sheet.rows
			sheet_cols = sheet.columns

			#make dictionaries for verification to ensure stuff goes in right place for all sheets
			row_storage[sheet_name] = []
			column_storage[sheet_name] = []
			for x in sheet_rows:
				row_storage[sheet_name].append((ID, x.id, x.row_number))
			for x in sheet_cols:
				column_storage[sheet_name].append((ID, x.id, x.index))

			#sets up the column and value the row's data should be added too
			for key, value in column_storage.items():
				for items in value:
					if ID == items[0]:
						if items[2] == int(self.ids.addrow_columnnumber.text) - 1:
							print(items[2])
							new_row = ss_client.models.Row()
							new_row.cells.append({
								'column_id': items[1],
								'value': self.ids.addrow_rowname.text
								})

			#sets up where the row will be added
			for key, value in row_storage.items():
				for items in value:
					if items[2]  == int(self.ids.addrow_rowabovenumber.text)  and ID == items[0]:
						if self.ids.addrowischild.active:
							if self.ids.topchildlist.active:
								new_row.parent_id = items[1]
							elif self.ids.bottomchildlist.active == 1:
								new_row.parent_id = items[1]
								new_row.to_bottom = True
						else:
							print(items[2])
							new_row.sibling_id = items[1]

			response = ss_client.Sheets.add_rows(
				ID, #sheet ID
				new_row)

			#print(sheet_name)
			
			#print(response)
	def update_row(self):
		#set up sheet variables
		for ID in selected_sheets_ID:
			sheet = ss_client.Sheets.get_sheet(ID)
			sheet_name = sheet.name
			sheet_rows = sheet.rows
			sheet_columns = sheet.columns
			# for containing row info
			row_storage = {}
			row_storage[sheet_name] = []
			for x in sheet_rows:
				row_storage[sheet_name].append((ID, x.id, x.row_number))
			# for containing column info
			col_storage = {}
			col_storage[sheet_name] = []
			for x in sheet_columns:
				col_storage[sheet_name].append((ID, x.id, x.index))
			# for containing cell info
			cell_storage = {}
			cell_storage[sheet_name] = []
			for x in sheet_rows:
				cells = x.cells
				for y in cells:
					for key, value in col_storage.items():
						for things in value:
							if y.column_id == things[1]:						
								cell_storage[sheet_name].append((ID, y.column_id, things[2] ,y.value))

			# constructs the cells to be inserted into row
			new_cells = {}
			for key, value in cell_storage.items():
				for cells in value:
							if cells[2] == int(self.ids.updaterowcolumnnumber.text) - 1:
								new_cells[str(cells[1])] = ss_client.models.Cell()
								new_cells[str(cells[1])].column_id = cells[1]
								new_cells[str(cells[1])].value = self.ids.updaterowcellvalue.text

			#adds the above cells to the row and sets the row position
			for key, value in row_storage.items():
				new_row = ss_client.models.Row()
				for rows in value:
					if int(self.ids.updaterowoldnumber.text) != int(self.ids.updaterownewrownumber.text):
						if int(self.ids.updaterowoldnumber.text) == rows[2]:
							new_row.id = rows[1]
							for key, value in new_cells.items():
								new_row.cells.append(value)

							if int(self.ids.updaterowoldnumber.text) == int(self.ids.updaterownewrownumber.text) -1:
								for k, v in row_storage.items():
									for rows in v:
										if int(self.ids.updaterownewrownumber.text) == rows[2]:
											new_row.sibling_id = rows[1]
		

							elif int(self.ids.updaterowoldnumber.text) == int(self.ids.updaterownewrownumber.text) + 1:
								for k, v in row_storage.items():
									for rows in v:
										if int(self.ids.updaterownewrownumber.text) - 1== rows[2]:
											new_row.sibling_id = rows[1]

							elif int(self.ids.updaterowoldnumber.text) < int(self.ids.updaterownewrownumber.text):
								for k, v in row_storage.items():
									for rows in v:
										if int(self.ids.updaterownewrownumber.text) == rows[2]:
											new_row.sibling_id = rows[1]				
							else:
								for k, v in row_storage.items():
									for rows in v:
										if int(self.ids.updaterownewrownumber.text) - 1 == rows[2]:
											new_row.sibling_id = rows[1]

							print("ROW MOVED to Row #: {}".format(self.ids.updaterownewrownumber.text))

						
					elif int(self.ids.updaterowoldnumber.text) == int(self.ids.updaterownewrownumber.text) and self.ids.indent.active == True:
						if int(self.ids.updaterowoldnumber.text) == rows[2]:
							new_row.id = rows[1]
							new_row.indent = 1
							for key, value in new_cells.items():
								new_row.cells.append(value)	
							print("ROW Updated and indented")
					elif int(self.ids.updaterowoldnumber.text) == int(self.ids.updaterownewrownumber.text) and self.ids.outdent.active == True:
						if int(self.ids.updaterowoldnumber.text)  == rows[2]:
							new_row.id = rows[1]
							new_row.outdent = 1
							for key, value in new_cells.items():
								new_row.cells.append(value)		
							print("ROW updated and outdented")
					elif int(self.ids.updaterowoldnumber.text) == int(self.ids.updaterownewrownumber.text):
						if int(self.ids.updaterowoldnumber.text) == rows[2]:
							new_row.id = rows[1]
							for key, value in new_cells.items():
								new_row.cells.append(value)	
							print("ROW updated")	

						#defines new row
			updated_row = ss_client.Sheets.update_rows(
				ID,
				new_row
				)
	def delete_row(self):
		#create row object
		
		row_storage = {}
		column_storage = {}
		for ID in selected_sheets_ID:
			#set up sheet/row data
			sheet = ss_client.Sheets.get_sheet(ID)
			sheet_name = sheet.name
			sheet_rows = sheet.rows

			#make dictionaries for verification to ensure stuff goes in right place for all sheets
			row_storage[sheet_name] = []
			for x in sheet_rows:
				row_storage[sheet_name].append((ID, x.id, x.row_number))


				if int(self.ids.deleterownumber.text) == x[2]:
					ss_client.Sheets.delete_rows(
						x[0], #sheet id
						x[1])
					print("Row #{} deleted".format(self.ids.delterownumber.text))
	
	def add_column(self):
		def column_type_chosen():
			popup = Popup(title='ERROR', content=Label(text='YOU MUST CHOOSE A COLUMN TYPE!!!'), auto_dismiss=False)
			if self.ids.checkbox.active:
				return "CHECKBOX"
			elif self.ids.contactlist.active:
				return "CONTACT_LIST"
			elif self.ids.date.active:
				return "DATE"
			elif self.ids.picklist.active:
				return "PICKLIST"
			elif self.ids.textnumber.active:
				return "TEXT_NUMBER"
			else:
				popup.open()

		if str(column_type_chosen()) == 'PICKLIST':
			column = ss_client.models.Column({
				'title': str(self.ids.addcolumntitle.text),
				'type': 'PICKLIST',
				'options': str(self.ids.addcolumnpicklist.text),
				'index': (int(self.ids.addcolumnnumber.text) -1),
				})
		else:
			column = ss_client.models.Column({
				'title': str(self.ids.addcolumntitle.text),
				'type': str(column_type_chosen()),
				'index': (int(self.ids.addcolumnnumber.text) - 1),
				})
		for ID in selected_sheets_ID:
			new_column = ss_client.Sheets.add_columns(
				ID,
				[column])
	def update_column(self):
		column_id = {}
		for ID in selected_sheets_ID:
			sheet = ss_client.Sheets.get_sheet(ID)
			sheet_cols = sheet.columns
			sheet_name = sheet.name
			column_id[sheet_name] = []
			for x in sheet_cols:
				column_id[sheet_name].append((ID, x.id, x.index, x.title, x.type))

		for key, value in column_id.items():
			for x in value:
				if int(self.ids.updatecolumnnewposition.text) - 1 == int(self.ids.updatecolumnposition.text) - 1:
					column_spec = ss_client.models.Column({
					'title': str(self.ids.updatecolumntitle.text),
					'type' : x[4],
					'index': int(self.ids.updatecolumnnewposition.text) - 1
					})
				elif int(self.ids.updatecolumnnewposition.text) - 1 != int(self.ids.updatecolumnposition.text) - 1:
					column_spec = ss_client.models.Column({
					'title': str(self.ids.updatecolumntitle.text),
					'type' : x[4],
					'index': int(self.ids.updatecolumnnewposition.text) -1
					})				
				if int(self.ids.updatecolumnposition.text) - 1 == x[2]:
					response = ss_client.Sheets.update_column(
						x[0],
						x[1],
						column_spec)

					updated_column = response.result
	def delete_column(self):
		column_id = {}
		for ID in selected_sheets_ID:
			sheet = ss_client.Sheets.get_sheet(ID)
			sheet_cols = sheet.columns
			sheet_name = sheet.name
			column_id[sheet_name] = []
			for x in sheet_cols:
				column_id[sheet_name].append((ID, x.id, x.index, x.title, x.type))	

			for key, value in column_id.items():
				for columns in value:
					if int(self.ids.deletecolumnposition.text) - 1 == columns[2]:
						ss_client.Sheets.delete_column(
							columns[0], #sheetid
							columns[1]   #column ID
							)

	def backup_sheets(self):
			workspace_sheets = ss_client.Workspaces.get_workspace(backup_workspace).sheets
			#keeps folder @ one backup at a time
			backed_up_sheets_ids = []
			for x in workspace_sheets:
				backed_up_sheets_ids.append(x.id)
				#first backup
			if len(backed_up_sheets_ids) == 0:
				for ID in selected_sheets_ID:
					sheet = ss_client.Sheets.get_sheet(ID)
					sheet_name = sheet.name

					response = ss_client.Sheets.copy_sheet(
						ID,
						ss_client.models.ContainerDestination({
							'destination_type': 'workspace',
							'destination_id': backup_workspace,
							'new_name': str(sheet_name)
							}),
						include= 'all')
			else:
				for ID in backed_up_sheets_ids:
					ss_client.Sheets.delete_sheet(ID)
				for ID in selected_sheets_ID:
					sheet = ss_client.Sheets.get_sheet(ID)
					sheet_name = sheet.name

					response = ss_client.Sheets.copy_sheet(
						ID,
						ss_client.models.ContainerDestination({
							'destination_type': 'workspace',
							'destination_id': backup_workspace,
							'new_name': str(sheet_name)
							}),
						include='all')

	global task_letters
	global used_task_letters
	global task_letters
	global used_task_letters
	def update_task_letters(self):
		for ID in selected_sheets_ID:
			task_letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
			used_task_letters = []		
			sheet = ss_client.Sheets.get_sheet(ID)
			sheet_rows = sheet.rows
			sheet_columns = sheet.columns

			for cols in sheet_columns:
				if cols.index == 2:
					task_column_id = cols.id
					
			for x in sheet_rows:
				#get temp data needed
				temp_row_number = x.row_number
				temp_row_parent_id = x.parent_id
				temp_row_id = x.id
				temp_row_cells = x.cells
				#build the cell
				new_cell = ss_client.models.Cell()
				new_cell.column_id = task_column_id
				row_has_REC = False
				#letter conditions
				#for first parent
				for cells in temp_row_cells:
					if cells.value == 'REC':
						row_has_REC = True

				if str(temp_row_parent_id) == 'None':
					if used_task_letters.count(task_letters[0]) > 0:
						task_letters.pop(0)
						new_cell.value = task_letters[0]
						used_task_letters.append(task_letters[0])
				#for later parents
					else:
						new_cell.value = task_letters[0]
						used_task_letters.append(task_letters[0])
				#for children
				else:
					if row_has_REC == True:
						if used_task_letters.count(task_letters[0]) > 0:
							task_letters.pop(0)
							new_cell.value = task_letters[0]
							used_task_letters.append(task_letters[0])
					else:
						new_cell.value = "{}.{}".format(task_letters[0], used_task_letters.count(task_letters[0]))
						used_task_letters.append(task_letters[0])
					

				new_row = ss_client.models.Row()
				new_row.id = temp_row_id
				new_row.cells.append(new_cell)

				updated_row = ss_client.Sheets.update_rows(
					ID,
					new_row)





	
class MassEditorApp(App):
	def build(self):
		screen_manager = ScreenManager(transition=FadeTransition())
		screen_manager.add_widget(WelcomeScreen(name="welcomescreen"))
		screen_manager.add_widget(WorkspaceSelection(name="workspaceselection"))
		screen_manager.add_widget(FolderSelection(name="folderselection"))
		screen_manager.add_widget(SheetSelection(name="sheetselection"))
		screen_manager.add_widget(Report(name="reportscreen"))
		screen_manager.add_widget(SheetEditor(name="sheeteditor"))
		return screen_manager

meapp = MassEditorApp()
meapp.run()