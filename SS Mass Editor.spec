# -*- mode: python -*-
from kivy.deps import sdl2, glew
block_cipher = None


a = Analysis(['SS_Mass_Editor.py'],
             pathex=['C:\\Users\\mpalu\\AppData\\Local\\Programs\\Python\\Python37-32\\Lib\\site-packages\\smartsheet_python_sdk-1.3.4.dev5+gd4d215c-py3.7.egg', 'C:\\Users\\mpalu\\Desktop\\SS_Editor_BUild'],
             binaries=[],
             datas=[
             ('C:\\Users\mpalu\Desktop\SS_Editor_BUild\API.txt', '.'),
             ('C:\\Users\mpalu\Desktop\SS_Editor_BUild\Backup.txt', '.'),
             ('C:\\Users\mpalu\Desktop\SS_Editor_BUild\MassEditor.kv', '.')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='SS Mass Editor',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True , icon='Icon.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               *[Tree(p) for p in (sdl2.dep_bins + glew.dep_bins)],
               strip=False,
               upx=True,
               name='SS Mass Editor')
